Author: Jelmer Vernooij <jelmer@debian.org>, Kenneth J. Pronovici <pronovic@debian.org>
Description: Add --no-include-build-time option to allow reproducible builds.
 This patch was contributed by the Debian Reproducible Builds effort [1].
 [1]: https://wiki.debian.org/ReproducibleBuilds
Bug: https://sourceforge.net/p/epydoc/bugs/367/
Bug-Debian: https://bugs.debian.org/783326
Forwarded: https://sourceforge.net/p/epydoc/bugs/367/
Last-Update: 2015-04-25
Index: epydoc-3.0.1+dfsg/epydoc/cli.py
===================================================================
--- epydoc-3.0.1+dfsg.orig/epydoc/cli.py
+++ epydoc-3.0.1+dfsg/epydoc/cli.py
@@ -137,7 +137,7 @@ OPTION_DEFAULTS = dict(
     include_source_code=True, pstat_files=[], simple_term=False, fail_on=None,
     exclude=[], exclude_parse=[], exclude_introspect=[],
     external_api=[], external_api_file=[], external_api_root=[],
-    redundant_details=False, src_code_tab_width=8)
+    redundant_details=False, src_code_tab_width=8, include_build_time=True)
 
 def parse_arguments():
     # Construct the option parser.
@@ -286,6 +286,10 @@ def parse_arguments():
         action='store_true', dest='include_log',
         help=("Include a page with the process log (epydoc-log.html)"))
 
+    generation_group.add_option('--no-include-build-time',
+        action='store_false', dest='include_build_time',
+        help=("Do not print the build time in the page footer, useful for reproducible builds."))
+
     generation_group.add_option(
         '--redundant-details',
         action='store_true', dest='redundant_details',
@@ -558,6 +562,8 @@ def parse_configfiles(configfiles, optio
             options.include_source_code = _str_to_bool(val, optname)
         elif optname in ('include-log', 'include_log'):
             options.include_log = _str_to_bool(val, optname)
+        elif optname in ('include-build-time', 'include_build_time'):
+            options.include_build_time = _str_to_bool(val, optname)
         elif optname in ('redundant-details', 'redundant_details'):
             options.redundant_details = _str_to_bool(val, optname)
 
Index: epydoc-3.0.1+dfsg/epydoc/docwriter/html.py
===================================================================
--- epydoc-3.0.1+dfsg.orig/epydoc/docwriter/html.py
+++ epydoc-3.0.1+dfsg/epydoc/docwriter/html.py
@@ -291,6 +291,9 @@ class HTMLWriter:
         @type include_log: C{boolean}
         @keyword include_log: If true, the the footer will include an
               href to the page 'epydoc-log.html'.
+        @type include_build_time: C{boolean}
+        @keyword include_build_time: If true, the the footer will
+              include the build time.
         @type src_code_tab_width: C{int}
         @keyword src_code_tab_width: Number of spaces to replace each tab
             with in source code listings.
@@ -358,6 +361,9 @@ class HTMLWriter:
         self._include_log = kwargs.get('include_log', False)
         """Are we generating an HTML log page?"""
 
+        self._include_build_time = kwargs.get('include_build_time', True)
+        """Are we including a build time?"""
+
         self._src_code_tab_width = kwargs.get('src_code_tab_width', 8)
         """Number of spaces to replace each tab with in source code
         listings."""
@@ -1770,10 +1776,14 @@ class HTMLWriter:
           <tr>
             <td align="left" class="footer">
         >>>   if self._include_log:
-            <a href="epydoc-log.html">Generated by Epydoc
-            $epydoc.__version__$ on $time.asctime()$</a>
-        >>>   else:
-            Generated by Epydoc $epydoc.__version__$ on $time.asctime()$
+            <a href="epydoc-log.html">
+        >>>   #endif
+            Generated by Epydoc $epydoc.__version__$
+        >>>     if self._include_build_time:
+            on $time.asctime()$
+        >>>     #endif
+        >>>   if self._include_log:
+            </a>
         >>>   #endif
             </td>
             <td align="right" class="footer">
Index: epydoc-3.0.1+dfsg/man/epydoc.1
===================================================================
--- epydoc-3.0.1+dfsg.orig/man/epydoc.1
+++ epydoc-3.0.1+dfsg/man/epydoc.1
@@ -226,6 +226,12 @@ Generate an HTML page
 .B epydoc\-log.html
 containing all error and warning messages that are generated by
 epydoc, and include it in the generated output.
+.TP
+.B \-\-no\-include\-build\-time
+Do not print the build time in the page footer.  This is useful if
+you are trying to generate reproducible builds, where each build
+against a given version of a source tree produces exactly the
+same artifacts.
 .RE
 .PP
 .\"--------------------------------------------------
